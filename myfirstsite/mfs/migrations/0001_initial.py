# Generated by Django 2.2.6 on 2019-10-04 07:03

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='City',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name_of_city', models.CharField(max_length=20)),
                ('name_of_state', models.CharField(max_length=20)),
                ('slug', models.SlugField()),
            ],
        ),
    ]

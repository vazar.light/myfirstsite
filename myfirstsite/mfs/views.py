from django.http import HttpResponseRedirect
from django.shortcuts import render
from . models import City
# Create your views here.

def index(request):
    st= City.objects.all()
    return render(request, "mfs/index.html", {"st": st})

def create(request):
    if request.method == "POST":
        cc = City()
        cc.name_of_city = request.POST.get("namecity")
        cc.name_of_state = request.POST.get("namestate")
        cc.save()
    return HttpResponseRedirect("/")
from django.db import models

# Create your models here.
class City(models.Model):
    name_of_city = models.CharField(max_length=20)
    name_of_state = models.CharField(max_length=20)
    slug = models.SlugField()

